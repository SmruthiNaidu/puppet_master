class ntpconfig {
service { 'ntpd':
  ensure  => running,
  require => Package[ntp],
}

package { 'ntp':
  ensure => present,
  before => Service['ntpd'],
}
}
