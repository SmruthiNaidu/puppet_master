class ambari_repo {
    file { "/etc/yum.repos.d/ambari.repo":
    content => template("ambari_repo/ambari.repo"),
    ensure  => file,
    mode => '644',
    owner => 'root',
    group => 'root',
  }
}
 
