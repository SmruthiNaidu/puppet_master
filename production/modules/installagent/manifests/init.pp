class installagent {
  $data = {
    '/home/stdlib' => { source => 'puppet:///modules/installagent/stdlib' },
  }


  create_resources(file,$data, {
    ensure => 'directory',
    recurse => 'remote',
    owner => 'root',
    group => 'root',
    mode  => '755',
  })

}

